# Q1
def q1():
    urls = [
        "http://www.baidu.com/a.txt",
        "http://www.google.com.tw/a.txt",
        "http://www.google.com/download/c.jpg",
        "https://www.google.co.jp/a.txt",
        "http://www.google.com/b.txt",
        "https://facebook.com/movie/b.txt",
        "http://yahoo.com/123/000/c.jpg"
    ];

    def count(urls):
        import re
        from collections import Counter

        regex = "[^/\\&\?]+\.\w{3,4}(?=([\?&].*$|$))"
        ans_list = []

        for url in urls:
            match = re.search(regex, url)
            try:
                ans_list.append(match.group(0))
            except:
                print('no file in url {}'.format(url))

        return dict(Counter(ans_list))

    print("Q1 ANS: {}".format(count(urls))) # {'a.txt': 3, 'b.txt': 2, 'c.jpg': 2}
    

def q2():


    def Q(n, r):
        if r == 0:
            return 1
        if r == n:
            return 1
        return Q(n-1, r) + Q(n-1, r-1)

    
    def C(n, r):
        memory = {}
        for ri in range(0,r+1):
            for ni in range(1,n+1):
                if ni == ri:
                    memory["{},{}".format(ni,ri)] = 1
                elif ri == 0:
                    memory["{},{}".format(ni,ri)] = 1
                elif ni < ri :
                    memory["{},{}".format(ni,ri)] = 0
                else:
                    memory["{},{}".format(ni,ri)] = (memory["{},{}".format(ni-1,ri)] + memory["{},{}".format(ni-1,ri-1)])
        return memory.get("{},{}".format(n,r))
    
    print("Q2 ANS: C({}, {}) = {}".format(900, 40, C(900,40)))


# Q3
def q3():
    a = [1, 2, 3, 4, 5, 5, 8]
    b = [1, 5, 8, 4]

    def inter(a,b):
        ans_list = list(set(a) & set(b))
        ans_list.sort()
        return ans_list

    print("Q3 ANS: {}".format(inter(a, b)))  # output [1, 4, 5, 8]

q1()
q2()
q3()